import java.util.Arrays;

/**
 * 
 */

/**
 * @author jbhilton
 *
 */
public class ArrayTestApp 
{

	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		/*
		double[] prices;
		prices = new double[4];
		*/
		
		//double [] prices = new double[4];
		
		// remember that Java (unlike MATLAB)
		// uses zero-based indexing
		/*
		prices[0] = 14.95;
		prices[1] = 12.95;
		prices[2] = 11.95;
		prices[3] = 9.95;
		*/
		
		// Don't attempt to access array elements less than 0
		// or greater than (the array length - 1);
		//prices[4] = 8.95; // this statement will compile,
							// but it will throw an 
							// ArrayIndexOutOfBoundsException
		
		double[] prices = { 14.95, 12.95, 11.95, 9.95 };
		
		// To get the length the name of an array, use arrayName.length
		
		System.out.println( "Length of the prices array: " + prices.length);
		
		int[] values = new int[10];
		
		System.out.println( Arrays.toString(values) );
		
		for (int i = 0; i < values.length; i++)
		{
			values[i] = i;
			
		} // end for
		
		System.out.println( Arrays.toString(values) );
		
		String[] strings = new String[3];
		
		System.out.println( Arrays.toString( strings ));
		
		// you can't do this!
		//strings = {"Hello", "Goodbye", "Hello again"};
		
		String[] moreStrings = {"Hello", "Goodbye", "Hello again"};
		
		System.out.println( Arrays.toString( moreStrings ));
		
		for ( int i = 0; i < moreStrings.length; i++ )
		{
			
			System.out.print( "\"" + moreStrings[i] + "\"");
			
			if (i < moreStrings.length - 1)
			{
				System.out.print(",");
				
			} // end if
			
		} // end for
		
		System.out.println();
		
		//declare an accumulator variable
		double sum = 0.0;
		
		for (int i=0; i < prices.length; i++)
		{
			sum = sum + prices[i]; // sum+= prices[i]
			System.out.println( i );
			
		} // end for
		
		double average = sum / prices.length;
		
		System.out.println( "Average of prices: $" + average);
		
		double sum2 = 0.0;
		
		// using an enhanced for loop..
		
		for( double price : prices) // "for each price in the prices array..."
		{
			sum2 = sum2 + price;
			price *= 2;
			
		}//end enhanced for
		
		double average2 = sum2/ prices.length;
		
		System.out.println( "Average of prices: $" + average2);
		System.out.println( Arrays.toString( prices ));
	
		int[] myArray = new int[2];
		myArray [0] = 1;
		myArray [1] = 2;
		
		System.out.println ("Before swapping in main: " + Arrays.toString(myArray) );
		
		// do the swap in the main method
		int temp = myArray[0];
		myArray[0] = myArray[1];
		myArray[1] = temp;
		
		System.out.println ("After swapping in main: " + Arrays.toString(myArray) );
		
		// we are passing value here, but the values passed
		// are copies of the elements stored in myArray[0]
		// and myArray[1] (it's as if we were passing the 
		// literal values 2 and 1)
	    
		swapUsingElements( myArray[0], myArray[1] );
		System.out.println( "After passing ELEMENTS to swap method: " + Arrays.toString(myArray ));
		
		// we are still passing by VALUE, but the value passed 
		// is a COPY of the REFERENCE to the array.
		swapUsingReferenceToArray( myArray );
		System.out.println( "After passing REFERENCE TO ARRAY to swap method: " + Arrays.toString(myArray ));
	} // end method main
	
	
	public static void swapUsingElements( int x, int y )
	{
		System.out.println( "Before Swap, x = " + x + ", y = " + y);
		int temp = x;
		x = y;
		y = temp;
		System.out.println( "After Swap, x = " + x + ", y = " + y);
		
	}// end method swapUsingElements
	
	public static void swapUsingReferenceToArray( int[] referenceToArray )
	{
		System.out.println( "Before Swap, referenceToArray[0] = " + referenceToArray[0] + 
							", referenceToArray[1] = " + referenceToArray[1]);
		
		int temp = referenceToArray[0];
		referenceToArray[0] = referenceToArray[1];
		referenceToArray[1] = temp;
		
		System.out.println( "After Swap, referenceToArray[0] = " + referenceToArray[0] + 
							", referenceToArray[1] = " + referenceToArray[1]);		
	}// end method swapUsingReferenceToArray
	
	

} // end class ArrayTestApp
